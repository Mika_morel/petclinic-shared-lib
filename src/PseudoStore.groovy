class PseudoStore {
	def data = [
		'mika' : 'Michaël Morel',
		'gilet' : 'Emmanuel Macron',
		'twitter' : 'Donald Trump'
	]

	String findNameByPseudo(String pseudo) {
		return data[pseudo]
	}
}
